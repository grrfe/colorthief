# Color Thief Fork

A fork of [Color Thief](https://github.com/SvenWoltmann/color-thief-java/) 
with the sole purpose of providing a version that is compatible with Java 9s module system, 
since the original author's tags aren't valid names.

* [License](LICENSE)
* [Written by](https://github.com/SvenWoltmann)